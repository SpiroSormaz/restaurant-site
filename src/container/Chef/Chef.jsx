import React from 'react';
import { images } from '../../constants';
import { SubHeading } from '../../components';
import './Chef.css';

const Chef = () => (
  <div className='app__bg app__wrapper section__padding'>
    <div className='app__wrapper_img app__wrapper_img-reverse'>
      <img src={images.chef} alt="chef-img" />
    </div>

    <div className='app__wrapper_info'>
      <SubHeading title="Chef's Word" />
      <h1 className='headtext_cormorant'>What we Belive In</h1>
    
    <div className='app__chef-content'>
      <div className='app__chef-content_quote'>
        <img src={images.quote} alt="quote" />
        <p className='p__opensans'>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque, tenetur possimus molestiae aperiam pariatur, sint molestias nesciunt delectus asperiores eos consequatur laboriosam aut facere non. Animi ipsum atque nisi ratione.</p>
      </div>
      <p className='p__opensans'>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad nulla eligendi vitae culpa architecto laudantium reprehenderit, sed odit tempora sit aperiam accusantium nostrum minima a dicta cumque? Assumenda, repudiandae possimus.</p>
    </div>

    <div className='app__chef-sign'>
      <p>Kevin Luo</p>
      <p className='p__opensans'>Chef & Founder</p>
      <img src={images.sign} alt="sign" />
    </div>

    </div>
  </div>
);

export default Chef;
