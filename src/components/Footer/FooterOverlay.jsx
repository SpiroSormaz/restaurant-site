import React from 'react';

import './FooterOverlay.css';

const FooterOverlay = () => (
  <div className='app__footerOverlay'>
    <div className='app__footerOvearlay-black' />
    <div className='app__footerOvearlay-img app__bg' />
  </div>
);

export default FooterOverlay;
